package com.example.gary.primerapruebaalexander;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    ProgressBar progressBar;
    Handler handler = new Handler();
    EditText editTextUsuario;
    TextView txtPorcenaje;
    Button btnLogin;
    Thread hilo;
    int i = 0;
    final String TAG = this.getClass().getName();
    public ArrAfiliadosLongitud arrAfiliadosLongitud;
    public static MainActivity mainActivity;
    boolean isActivo = false;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextUsuario = (EditText)findViewById(R.id.edtUsuario);
        btnLogin = (Button)findViewById(R.id.btnLogin);

      
        txtPorcenaje = (TextView)findViewById(R.id.txtPorcentaje);
        progressBar = (ProgressBar)findViewById(R.id.determinateBar);
        mainActivity = this;
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                 
                   validarUsuario("http://192.168.0.119:8888/Postgresql.php");
                }catch (Exception e){

                    e.printStackTrace();
                }
            }
        });
    }
    private void validarUsuario(String URL){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               Gson gson = new Gson();
                arrAfiliadosLongitud = gson.fromJson(response, ArrAfiliadosLongitud.class);
                int longitudArray = Integer.parseInt(String.valueOf(arrAfiliadosLongitud.getLongitud()));
                if (longitudArray != 0) {
                    try {
                        if (!arrAfiliadosLongitud.getArrAfiliados()[0].getMatricula().isEmpty()){
                            if (arrAfiliadosLongitud.getArrAfiliados()[0].getEstado() == 2) {
                              
                                Toast.makeText( MainActivity.this, "Usted no esta activo, verifique con el administrador", Toast.LENGTH_SHORT).show();
                            }else {

                                if (!isActivo) {
                                    hilo = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            while (i <= 100){
                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        String porcentaje = i+" %";
                                                        txtPorcenaje.setText(porcentaje);
                                                        progressBar.setProgress(i);
                                                    }
                                                });
                                                try {
                                                    Thread.sleep(100);
                                                }catch (InterruptedException e){
                                                    e.printStackTrace();
                                                }
                                                if (i == 100){
                                                    Intent intent = new Intent(getApplicationContext(), MenuSlideActivity.class);
                                                    Bundle bundle = new Bundle();
                                                    bundle.putSerializable("", arrAfiliadosLongitud);
                                                    intent.putExtras(bundle);
                                                    startActivity(intent);
                                                }
                                                i =  i + 20;
                                                isActivo = true;
                                            }
                                        }
                                    });
                                    hilo.start();
                                }
                            }
                        }
                    }catch (Exception e) {
                        Toast.makeText( MainActivity.this, "No se encuentra registrado", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText( MainActivity.this, "No se encuentra registrado", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyError cause = (VolleyError) error.getCause();
           
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parametros = new HashMap<String, String>();
                parametros.put("matricula",editTextUsuario.getText().toString());
                return parametros;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(MainActivity.this);
        alerta.setMessage("Desea salir de la aplicación").setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog titulo = alerta.create();
        titulo.setTitle("Salida");
        titulo.show();
    }
}
