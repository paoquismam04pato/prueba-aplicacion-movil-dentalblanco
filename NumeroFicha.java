package com.example.gary.primerapruebaalexander;
import java.io.Serializable;

public class NumeroFicha implements Serializable
{
    private String longitudFicha;
    private Ficha arrFicha[];
    private String nombre;
    private String paterno;
    private String materno;
    private String matricula;

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    public String getPaterno() { return paterno; }
    public void setPaterno(String paterno) { this.paterno = paterno; }
    public String getMaterno() { return materno; }
    public void setMaterno(String materno) { this.materno = materno; }
    public String getMatricula() { return matricula; }
    public void setMatricula(String matricula) { this.matricula = matricula; }
    public NumeroFicha(Ficha[] arrFicha) { this.arrFicha = arrFicha; }

    // TODO SerGio Morga Getters and Setters
    public Ficha[] getArrFicha() { return arrFicha; }
    public void setArrFicha(Ficha[] arrFicha) { this.arrFicha = arrFicha; }
    public String getLongitudFicha() { return longitudFicha; }
    public void setLongitudFicha(String longitudFicha) { this.longitudFicha = longitudFicha; }
}