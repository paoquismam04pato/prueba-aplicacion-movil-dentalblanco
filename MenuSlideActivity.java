package com.example.gary.primerapruebaalexander;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

public class MenuSlideActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public ArrAfiliadosLongitud arrayAfiliadosLongitud;
    public ArrayAfiliadosMasBeneficiarios arrayAfiliadosMasBeneficiarios;
    public ArraAfiliadosBeneficiariosCitasMedicas arraAfiliadosBeneficiariosCitasMedicas;
    public NumeroFicha arrFicha;
    private MainActivity claseInicial = new MainActivity();
    private MiTareaAsincronaDialog asincronoActualizarDatos;
    private MiTareaAsincronaCitas asincronoCitasMedicas;
    // private CitasMedicasAsincronaDialog asincronoCitasMedicas;
    public ProgressDialog pDialog;
    public static MenuSlideActivity activityMenu;

    public ArrayAfiliadoBeneficiarioEspecialidadesMedicas arrayAfiliadoBeneficiarioEspecialidadesMedicas;
    // TODO SerGio para la configuracion de la pantalla girada
    private ActionBarDrawerToggle mDrawerToggle;

    // TODO Inicio del primer fragmento:
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_slide);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // TODO Sergio Se inicia el primer fragmento
        activityMenu = this;
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.escenario, new InicioFragment()).commit();

        /*if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ) {
            Toast.makeText(this,"Estamos en Portrait Mode",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this,"Estamos en Landscape Mode",Toast.LENGTH_SHORT).show();
        }*/
        /*MenuItem item;
        int id = item.getItemId();
        switch (R.id.action_settings)
        {
            case R.id.escenario: fragmentManager.beginTransaction().replace(R.id.escenario, new InicioFragment()).commit();
                break;
            case R.id.idActualizarDatos:
                // Con ejecucion con hilos
                pDialog = new ProgressDialog(MenuSlideActivity.this);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setMessage("Procesando...");
                pDialog.setCancelable(true);
                pDialog.setMax(100);
                j = new MiTareaAsincronaDialog();
                j.execute();
                // /actualizarDatos();
                break;
            default:fragmentManager.beginTransaction().replace(R.id.escenario, new InicioFragment()).commit();
                break;
        }*/
        // TODO Sergio La siguiente linea de codigo es para dibujar el menu
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        // TODO SerGio Las siguientes lineas es para seleccionar el item y visualizarlo en pantalla
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_slide, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            startActivity(new Intent(getBaseContext(), MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
            claseInicial.mainActivity.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        //detectamos el cambio de orientación en este caso

    // LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //  int id = item.getItemId();
   // View newView = inflater.inflate(R.layout.fragment_historial, null);

       // MenuBuilder menuBuilder = new MenuBuilder(newConfig.);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // newConfig.
            // TODO Eliminar una vez revisado
            // android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            // fragmentManager.beginTransaction().replace(R.id.id_inicio, new InicioFragment()).commit();
            // mDrawerToggle.onConfigurationChanged(newConfig);
           //  Toast.makeText(this, "landscape ", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//            Toast.makeText(this, "portrait " , Toast.LENGTH_SHORT).show();
        }
        /*if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            landscape = true;
            //acciones deseadas
        }

        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            landscape = false;
            //acciones deseadas
        }*/
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        switch (id) {
            case R.id.id_inicio:
                fragmentManager.beginTransaction().replace(R.id.escenario, new InicioFragment()).commit();
                break;
            case R.id.idActualizarDatos:
                // TODO Con ejecucion con hilos
                pDialog = new ProgressDialog(MenuSlideActivity.this);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setMessage("Procesando...");
                pDialog.setCancelable(true);
                pDialog.setMax(100);
                asincronoActualizarDatos = new MiTareaAsincronaDialog();
                asincronoActualizarDatos.execute();
                break;
            case R.id.citasMedicas:
               // CitaMedica();
                pDialog = new ProgressDialog(MenuSlideActivity.this);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setMessage("Procesando...");
                pDialog.setCancelable(true);
                pDialog.setMax(100);
                asincronoCitasMedicas = new MiTareaAsincronaCitas();
                asincronoCitasMedicas.execute();
                //CitaMedica();
                break;
            case R.id.historial:
                // TODO pendiente realizar consulta en mysql

                //Toast.makeText(getApplicationContext(), "Entra aqui asdas", Toast.LENGTH_SHORT).show();
                cargarHistorial();
                break;
            case R.id.id_cerrar_session:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                startActivity(new Intent(getBaseContext(), MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();
                claseInicial.mainActivity.finish();
                break;
            default: break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void CitaMedica()
    {
        // TODO SerGio URL
        String URL = "http://192.168.0.119:8888/cajaSaludCaminos/citaMedica.php";
        // String URL = "http://192.168.226.119:8888/cajaSaludCaminos/citaMedica.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                Gson gson = new Gson();
                arrayAfiliadoBeneficiarioEspecialidadesMedicas = gson.fromJson(response, ArrayAfiliadoBeneficiarioEspecialidadesMedicas.class);


                String cantidad = arrayAfiliadoBeneficiarioEspecialidadesMedicas.getCantidad();
                String cantidadDia = arrayAfiliadoBeneficiarioEspecialidadesMedicas.getCantidadDia();
                // Toast.makeText(MenuSlideActivity.this, "canditda: " + cantidad + " Cantidad Dia: "+ cantidadDia, Toast.LENGTH_LONG).show();
               if (Integer.parseInt(cantidad) <= 5  && Integer.parseInt(cantidadDia) < 1)
                {
                    String strParenteso = "";
                    int longitudBeneficiarios = Integer.parseInt(String.valueOf(arrayAfiliadoBeneficiarioEspecialidadesMedicas.getLongitudBeneficiariosParentesco()));
                    for (int i = 0; i< longitudBeneficiarios; i++){
                        strParenteso = strParenteso + "" + arrayAfiliadoBeneficiarioEspecialidadesMedicas.getArrBeneficiarios()[i].getPaternoBeneficiario() + " " + arrayAfiliadoBeneficiarioEspecialidadesMedicas.getArrBeneficiarios()[i].getMaternoBeneficiario() + " "+ arrayAfiliadoBeneficiarioEspecialidadesMedicas.getArrBeneficiarios()[i].getNombreBeneficiario() + "  " + arrayAfiliadoBeneficiarioEspecialidadesMedicas.getArrBeneficiarios()[i].getMatriculaBeneficiario() + "\n";
                    }
                    android.support.v4.app.FragmentTransaction ftCitasMedicas = getSupportFragmentManager().beginTransaction();
                    CitasMedicasFragment fragmentCitaMedica = CitasMedicasFragment.newInstance(arrayAfiliadoBeneficiarioEspecialidadesMedicas, strParenteso, longitudBeneficiarios);
                    ftCitasMedicas.replace(R.id.escenario, fragmentCitaMedica);
                    ftCitasMedicas.commit();
                }else{
                    // TODO SerGio TOAST Personalizado // El Layout Inflater es para crear un layout personalizado
                    LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
                    View customToast = inflater.inflate(R.layout.custom_toast, null);
                    TextView txt = (TextView)customToast.findViewById(R.id.idTxtToast);
                    txt.setText("FICHAJE ESPECIALIDADES MEDICAS - DOBLE ATENCIÓN");
                    Toast toast = new Toast(MenuSlideActivity.this);
                    toast.setGravity(Gravity.CENTER, 0,0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(customToast);
                    toast.show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MenuSlideActivity.this, "Error al conectar al servidor: ", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parametros = new HashMap<String, String>();
                Bundle objEnviado = getIntent().getExtras();
                assert objEnviado != null;
                arrayAfiliadosLongitud = (ArrAfiliadosLongitud) objEnviado.getSerializable("arrAfiliado");
                assert arrayAfiliadosLongitud != null;
                String matricula = arrayAfiliadosLongitud.getArrAfiliados()[0].getMatricula();
                String idRegional = arrayAfiliadosLongitud.getArrAfiliados()[0].getId_rgl();
                parametros.put("matricula", matricula);
                parametros.put("id_rgl", idRegional);
                return parametros;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void cargarHistorial()
    {
        // TODO SerGio URL
        String URL = "http://192.168.0.119:8888//cajaSaludCaminos/HistorialMedico.php";
        //String URL = "http://192.168.226.119:8888/cajaSaludCaminos/HistorialMedico.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                arrFicha = gson.fromJson(response, NumeroFicha.class);
                //arrFicha.getLongitudFicha();
                // Toast.makeText(MenuSlideActivity.this, "Entra aqui"+ arrFicha.getLongitudFicha(), Toast.LENGTH_LONG).show();

                android.support.v4.app.FragmentTransaction HistorialMedico = getSupportFragmentManager().beginTransaction();
                HistorialFragment fragmentHistorial = HistorialFragment.newInstance(arrFicha);
                HistorialMedico.replace(R.id.escenario, fragmentHistorial);
                HistorialMedico.commit();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MenuSlideActivity.this, "Error al conectar al servidor: ", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parametros = new HashMap<String, String>();
                Bundle objEnviado = getIntent().getExtras();
                assert objEnviado != null;
                // TODO El Array Afiliado solo me devuelve el arrar de los datos con su nombre apellido ojo no me devuelve varios afiliados
                arrayAfiliadosLongitud = (ArrAfiliadosLongitud) objEnviado.getSerializable("arrAfiliado");
                String matricula = arrayAfiliadosLongitud.getArrAfiliados()[0].getMatricula();
                parametros.put("matricula", matricula);
                return parametros;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void actualizarDatos()
    {
        // TODO SerGio URL

        //String URL = "http://192.168.226.119:8888/cajaSaludCaminos/Postgresql.php";
        String URL = "http://192.168.0.119:8888/cajaSaludCaminos/Postgresql.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                String strParenteso = "";
                arrayAfiliadosMasBeneficiarios = gson.fromJson(response, ArrayAfiliadosMasBeneficiarios.class);
                int longitudCantidadParentesco = Integer.parseInt(String.valueOf(arrayAfiliadosMasBeneficiarios.getLongitudBeneficiariosParentesco()));
                for (int i = 0; i< longitudCantidadParentesco; i++){
                    strParenteso = strParenteso + "" + arrayAfiliadosMasBeneficiarios.getArrBeneficiarios()[i].getPaternoBeneficiario() + " " + arrayAfiliadosMasBeneficiarios.getArrBeneficiarios()[i].getMaternoBeneficiario() + " "+ arrayAfiliadosMasBeneficiarios.getArrBeneficiarios()[i].getNombreBeneficiario() +" "+ arrayAfiliadosMasBeneficiarios.getArrBeneficiarios()[i].getMatriculaBeneficiario() + "\n";
                }

                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ActualizarDatosFragment fragment = ActualizarDatosFragment.newInstance(arrayAfiliadosMasBeneficiarios.getArrAfiliados()[0], strParenteso, longitudCantidadParentesco);
                ft.replace(R.id.escenario, fragment);
                ft.commit();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MenuSlideActivity.this, "Error al conectar al servidor: ", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parametros = new HashMap<String, String>();
                Bundle objEnviado = getIntent().getExtras();
                arrayAfiliadosLongitud = (ArrAfiliadosLongitud) objEnviado.getSerializable("arrAfiliado");
                String matricula = arrayAfiliadosLongitud.getArrAfiliados()[0].getMatricula();
                parametros.put("matricula", matricula);
                return parametros;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    // TODO Tarea Asincrona para actualizar Datos
    private class MiTareaAsincronaDialog extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            actualizarDatos();
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pDialog.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {

            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    MiTareaAsincronaDialog.this.cancel(true);
                }
            });

            pDialog.setProgress(0);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                pDialog.dismiss();
                View customToast = getLayoutInflater().inflate(R.layout.mensaje_historial, null);
                TextView txt = (TextView)customToast.findViewById(R.id.idTxthistorial);
                txt.setText("Procesando...");
                Toast toast = new Toast(MenuSlideActivity.this);
                // toast.setGravity(Gravity.CENTER, 0,0);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(customToast);
                toast.show();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MenuSlideActivity.this, "",
                    Toast.LENGTH_SHORT).show();
        }
    }


    // TODO Tarea Asincrona para Citas Medicas
    private class MiTareaAsincronaCitas extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            CitaMedica();
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pDialog.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {

            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    MiTareaAsincronaCitas.this.cancel(true);
                }
            });

            pDialog.setProgress(0);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                pDialog.dismiss();
                View customToast = getLayoutInflater().inflate(R.layout.mensaje_historial, null);
                TextView txt = (TextView)customToast.findViewById(R.id.idTxthistorial);
                txt.setText("Procesando...");
                Toast toast = new Toast(MenuSlideActivity.this);
                // toast.setGravity(Gravity.CENTER, 0,0);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(customToast);
                toast.show();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MenuSlideActivity.this, "",
                    Toast.LENGTH_SHORT).show();
        }
    }


    // TODO Tarea Asincrona para citas medicas
    private class CitasMedicasAsincronaDialog extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            CitaMedica();
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pDialog.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    CitasMedicasAsincronaDialog.this.cancel(true);
                }
            });
            pDialog.setProgress(0);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                pDialog.dismiss();
                Toast.makeText(MenuSlideActivity.this, "Procesando...", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MenuSlideActivity.this, "",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Todo SerGio Cerrar MenuSlideActivity
    @Override
    public void onBackPressed() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(MenuSlideActivity.this);
        alerta.setMessage("Desea cerrar la sesión").setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        startActivity(new Intent(getBaseContext(), MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                        finish();
                        claseInicial.mainActivity.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog titulo = alerta.create();
        titulo.setTitle("Salida");
        titulo.show();
    }
}
