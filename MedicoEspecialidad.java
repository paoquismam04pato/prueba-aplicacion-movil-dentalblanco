package com.example.gary.primerapruebaalexander;

import java.io.Serializable;

/**
 * Created by Gary on 26/01/2020.
 */

public class MedicoEspecialidad implements Serializable
{
    public String id_especialidad;
    public String descripcion;
    public String total_ficha;
    public String icon_identifier;
    public String estado;
    public String id_regional;
    public String total_disponible;

    public String getId_regional() {
        return id_regional;
    }

    public void setId_regional(String id_regional) {
        this.id_regional = id_regional;
    }

    public String getTotal_disponible() {
        return total_disponible;
    }

    public void setTotal_disponible(String total_disponible) {
        this.total_disponible = total_disponible;
    }

    public MedicoEspecialidad(String id_especialidad, String descripcion, String total_ficha, String icon_identifier, String estado, String id_regional, String total_disponible) {
        this.id_especialidad = id_especialidad;
        this.descripcion = descripcion;
        this.total_ficha = total_ficha;
        this.icon_identifier = icon_identifier;
        this.estado = estado;
        this.id_regional = id_regional;
        this.total_disponible = total_disponible;
    }

    public String getId_especialidad() {
        return id_especialidad;
    }

    public void setId_especialidad(String id_especialidad) {
        this.id_especialidad = id_especialidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTotal_ficha() {
        return total_ficha;
    }

    public void setTotal_ficha(String total_ficha) {
        this.total_ficha = total_ficha;
    }

    public String getIcon_identifier() {
        return icon_identifier;
    }

    public void setIcon_identifier(String icon_identifier) {
        this.icon_identifier = icon_identifier;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdRegional() {
        return id_regional;
    }

    public void setIdRegional(String id_regional) {
        this.id_regional = id_regional;
    }
}
